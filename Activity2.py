#Question 1
print("Twinkle, twinkle, little star,")
print("\n\tHow I wonder what you are! \n\t\tUp above the world so high,")
print("\n\t\tLike a diamond in the sky. \nTwinkle, twinkle, little star,")
print("\n\tHow I wonder what you are!")

#Question 2
import sys
print("Python version")
print (sys.version)

#Question 3
a = "sample string"
print(len(a))

#Question 4
name = input("Input Name: ")
print("Your name is " + name)
age = input("Input Age: ")
print("You are " + age + " years old")

#Question 5
import datetime
x = datetime.datetime.now()
print(x)

#Question 6
inputs = input("Input Data: ")
thislist = list(inputs)
thistuple = tuple(inputs)
print(thislist)
print(thistuple)

#Question 7
color_list = ["Red", "Green", "White", "Black"]
for x in color_list:
  if x == "Red": print(x)
  elif x == "Black": print(x)

#Question 8
def my_function(message):
	return message

myList = ("Sample", "Python", "Message")

for x in myList:
  if x == "Python":
    break
  print(" ".join(myList))
    
print(my_function(myList))

my_function(myList)


#Question 9
n = "5"
print(int(n) + int(n + n) + int(n + n + n))
  

