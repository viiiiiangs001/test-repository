#Question 1
from datetime import date
fDate = date(2014, 7, 2)
lDate = date(2014, 7, 11)
diff = lDate + fDate
print(str(diff.days) + " days")

#Question 2
fNum = 0
fNum = input("Enter number: ")
if int(fNum) % 2 != 0:
  print("The number is Odd")
else:
  print("The number is Even")

#Question 3
def checkNum(n):
  return ((abs(100 - n) <= 100) 
  or (abs(1000 - n) <= 100) or (abs(2000 - n) <= 100))

num = 0
num = input("Enter Number to be checked: ")

print(checkNum(int(num)))

#Question 4
amount = 10000
interest = 3.5
years = 7
future_val = amount * ((1+(0.01*interest)) ** years)
print(round(future_val, 2))

#Question 5
def change_char(strng):
  char = strng[0]
  strng = strng.replace(char, '$')
  strng = char + strng[1:]

  return strng

print(change_char('restart'))

#Question 6
import datetime

def checkParam(param1, param2):
  
  if type(param1) == int and type(param2) == int:
    print(param1 * param2)
    return param1, param2
  elif type(param1) == str and type(param2) == str:
    print(param1 + " " + param2)
    return param1, param2
  elif type(param1) == list and type(param2) == list:
    print(param1 + param2)
    return param1, param2
  elif type(param1) == dict and type(param2) == dict:
    print(param1," , ", param2)
    return param1, param2
  elif type(param1) == datetime.datetime and type(param2) == datetime.datetime:
    print(param1, " : ", param2)
    return param1, param2
  
  return param1, param2

checkParam(10, 2)
checkParam("Hello", "World")
checkParam(["Hello", "World"], ["Welcome", "Python"])
checkParam({"brand": "Ford", "model": "Mustang", "year": 1964 }
, {"brand": "Toyota", "model": "Corola", "year": 1995 })
checkParam(datetime.datetime(2020, 5, 17), datetime.datetime.now())

#Question 7
class Person:
  def __init__(name, first_name, middle_name, last_name):
    name.first_name = first_name
    name.middle_name = middle_name
    name.last_name = last_name

  def get_full_name(name):
    fullname = name.first_name + " " + name.middle_name + " " + name.last_name
    print(fullname)
    return fullname

fname = input("Input First Name: ")
mname = input("Input Middle Name: ")
lname = input("Input Last Name: ")

person1 = Person(fname, mname, lname)
person1.get_full_name()

#Question 8
class Parent:
  def __init__(name, first_name, middle_name, last_name):
    name.first_name = first_name
    name.middle_name = middle_name
    name.last_name = last_name

  def get_full_name(name):
    fullname = name.first_name + " " + name.middle_name + " " + name.last_name
    
class Child(Parent):
  def __init__(name, first_name, middle_name, last_name):
    super().__init__(first_name, middle_name, last_name)
    
  def get_full_name(name):
    fullname = name.first_name + " " + name.middle_name + " " + name.last_name
    return print(fullname.upper())

fname = input("Input First Name: ")
mname = input("Input Middle Name: ")
lname = input("Input Last Name: ")

child1 = Child(fname, mname, lname)
child1.get_full_name()

#Question 9
class Cars:
    def __init__(car, name, model, type, price):
      car.name = name
      car.model = model
      car.type = type
      car.price = price

    def get_car_details(car):
      vehicle =  car.name, car.model, car.type, car.price
      
      return vehicle

class Vehicle(Cars):
  def __init__(car, name, model, type, price):
    super().__init__(name, model, type, price)

  def get_car_details(car):
    vehicle_details = car.name, car.model, car.type, car.price
    return print(vehicle_details)

cName = input("Input Car Name: ")
cModel = input("Input Car Model: ")
cType = input("Input Car Type: ")
cPrice = input("Input Car Price: ")

vehicle1 = Vehicle(cName, cModel, cType, cPrice)
vehicle1.get_car_details()

